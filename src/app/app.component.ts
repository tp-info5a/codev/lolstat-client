import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {faSignInAlt, faSignOutAlt, faStar, faUserPlus} from '@fortawesome/free-solid-svg-icons';
import {SummonerService} from './services/summoner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lolstat-client';
  hasToken: boolean;

  summonerName: string;

  faStar = faStar;
  faSignOutAlt = faSignOutAlt;
  faSignInAlt = faSignInAlt;
  faUserPlus = faUserPlus;

  constructor(private router: Router, private summonerService: SummonerService) {
    this.hasToken = window.localStorage.getItem('token') != null;
  }

  logout(): void {
    window.localStorage.removeItem('token');
    location.reload();
  }

  getSummoner(): void {
    this.router.navigateByUrl('/summoner/' + this.summonerName);
  }
}
