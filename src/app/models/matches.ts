export class Matches {
  size: number;
  content: Array<Game>;


  constructor() {
    this.content = [];
  }
}

export class Game {
  platformId: string;
  gameId: number;
  champion: number;
  queue: number;
  season: number;
  timestamp: number;
  role: string;
  lane: string;
}

export class MatchDetail {
  gameId: number;
  queue: number;
  gameType: string;
  gameDuration: number;
  platformId: string;
  gameCreation: number;
  seasonId: number;
  gameVersion: string;
  mapId: number;
  gameMode: string;
  participantIdentities: Array<ParticipantIdentity>;
  teams: Array<TeamStats>;
  participants: Array<Participant>;

  constructor() {
    this.participantIdentities = [];
    this.teams = [];
    this.participants = [];
  }
}

export class ParticipantIdentity{
  participantId: number;
  player: Player;

  constructor() {
    this.player = new Player();
  }
}

export class Player {
  profileIcon: number;
  accountId: string;
  matchHistoryUri: string;
  currentAccountId: string;
  currentPlatformId: string;
  summonerName: string;
  summonerId: string;
  platformId: string;


  constructor() {}
}

export class TeamStats {}

export class Participant {
  participantId: number;
  championId: number;
  stats: ParticipantStat;

  constructor() {
    this.stats = new ParticipantStat();
  }
}

export class ParticipantStat {
  item0: number;
  item1: number;
  item2: number;
  item3: number;
  item4: number;
  item5: number;
  item6: number;
  win: boolean;
  kills: number;
  deaths: number;
  assists: number;
  goldEarned: number;

  constructor() {
  }
}
