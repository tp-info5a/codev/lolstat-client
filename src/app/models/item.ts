export class Item {
  id: number;
  name: string;
  description: string;
  gold: ItemGold;
  imageUrl: string;


  constructor() {
    this.gold = new ItemGold();
  }
}

export class ItemGold {
  base: number;
  total: number;

  constructor() {}
}

export class ItemBlock {
  type: string;
  showIfSummonerSpell: string;
  items: Array<Item>;
}

export class ItemForChampion {
  map: string;
  mode: string;
  type: string;
  blocks: Array<ItemBlock>;
}
