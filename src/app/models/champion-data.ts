import {ItemForChampion} from './item';

export class ChampionData {
  id: string;
  key: string;
  name: string;
  imageUrl: string;
  title: string;
  lore: string;
  allytips: Array<string>;
  enemytips: Array<string>;
  tags: Array<string>;
  partype: string;
  info: ChampionDataInfo;
  passive: ChampionDataPassive;
  spells: Array<ChampionDataSpell>;
  recommended: Array<ItemForChampion>;
  skins: Array<ChampionDataSkin>;


  constructor() {
    this.info = new ChampionDataInfo();
    this.passive = new ChampionDataPassive();
    this.spells = [];
    this.recommended = [];
    this.skins = [];
  }
}

export class ChampionDataInfo {
  attack: number;
  defence: number;
  magic: number;
  difficulty: number;
}

export class ChampionDataPassive {
  name: string;
  description: string;
  imageUrl: string;
}

export class ChampionDataSpell {
  id: string;
  name: string;
  description: string;
  tooltip: string;
  cooldownBurn: string;
  costBurn: string;
  effectBurn: Array<string>;
  vars: Array<ChampionDataSpellVar>;
  rangeBurn: string;
  imageUrl: string;
}

export class ChampionDataSpellVar {
  link: string;
  // coeff: number;
  key: string;
}

export class ChampionDataSkin {
  num: number;
  name: string;
  imageUrl: string;
}
