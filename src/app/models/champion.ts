export class Champion {
  id: number;
  name: string;
  imageSquare: string;
  free: boolean;
}
