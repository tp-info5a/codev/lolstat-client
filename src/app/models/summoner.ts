import {Champion} from './champion';

export class Summoner {
  id: string;
  accountId: string;
  puuid: string;
  name: string;
  profileIconId: number;
  summonerLevel: number;

  constructor() {}
}

export class SummonerLight {
  name: string;
  profileIconId: number;

  constructor() {}
}

export class SummonerMasteries {
  champion: Champion;
  championLevel: number;
  championPoints: number;
  lastPlayTime: number;
  championPointsSinceLastLevel: number;
  championPointsUntilNextLevel: number;
  chestGranted: boolean;
  tokensEarned: number;
  summonerId: string;

  constructor() {
    this.champion = new Champion();
  }
}
