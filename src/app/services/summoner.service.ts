import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Summoner, SummonerMasteries} from '../models/summoner';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {MatchDetail, Matches} from '../models/matches';
import {Item} from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class SummonerService {

  constructor(private http: HttpClient) { }

  public getSummonerByName(summonerName: string): Observable<Summoner> {
    return this.http.get<Summoner>(environment.url_api + 'summoner/get-by-name/' + summonerName, environment.httpOptions);
  }

  public getSummonerMasteries(summonerId: string): Observable<Array<SummonerMasteries>> {
    return this.http.get<Array<SummonerMasteries>>(environment.url_api + 'summoner/get-mastery/' + summonerId, environment.httpOptions);
  }

  public getSummonerMatchHistory(accountId: string, page: number): Observable<Matches> {
    return this.http.get<Matches>(environment.url_api + 'summoner/get-history/' + accountId + '/' + page, environment.httpOptions);
  }

  public getMatchDetail(matchId: number): Observable<MatchDetail> {
    return this.http.get<MatchDetail>(environment.url_api + 'match/detail/' + matchId, environment.httpOptions);
  }

  public getItem(itemId: number): Observable<Item> {
    return this.http.get<Item>(environment.url_api + 'match/get-item/' + itemId, environment.httpOptions);
  }

  public getMapName(mapId: number): Observable<any> {
    return this.http.get<string>(environment.url_api + 'match/get-map/' + mapId, environment.httpOptions);
  }
}
