import { TestBed } from '@angular/core/testing';

import { ChampionService } from './champion.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('ChampionService', () => {
  let service: ChampionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ChampionService, HttpClient]
    });
    service = TestBed.inject(ChampionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
