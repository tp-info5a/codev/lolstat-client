import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Champion} from '../models/champion';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ChampionData} from '../models/champion-data';

@Injectable({
  providedIn: 'root'
})
export class ChampionService {

  constructor(private http: HttpClient) { }

  public getAllChampion(): Observable<Champion[]> {
    return this.http.get<Champion[]>(environment.url_api + 'champion/all/', environment.httpOptions);
  }

  getChampionByName(championName: string): Observable<ChampionData> {
    return this.http.get<ChampionData>(environment.url_api + 'champion/by-name/' + championName, environment.httpOptions);
  }

  getChampionById(championId: number): Observable<Champion> {
    return this.http.get<Champion>(environment.url_api + 'champion/by-id/' + championId, environment.httpOptions);
  }
}
