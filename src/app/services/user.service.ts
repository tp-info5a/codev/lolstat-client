import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RegisterUser, SignInUser} from '../models/user';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Champion} from '../models/champion';
import {Summoner} from '../models/summoner';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private getHttpOptionsWithAuthorization(): { headers: HttpHeaders } {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: 'Bearer ' + window.localStorage.getItem('token'),
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:4200/'
      })
    };

    console.log('httpOptions', httpOptions);

    return httpOptions;
  }

  public registerUser(newUser: RegisterUser): Observable<any> {
    return this.http.post<any>(
      environment.url_api + 'login/new-user/',
      newUser,
      environment.httpOptions
    );
  }

  public signInUser(user: SignInUser): Observable<any> {
    return this.http.post<any>(
      environment.url_api + 'login/sign-in-user/',
      user,
      environment.httpOptions
    );
  }

  public checkIfFavoriteChampion(championId: string): Observable<boolean> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.post<boolean>(
      environment.url_api + 'user/check-favorite-champion',
      championId,
      httpOptionsWithAuthorization
    );
  }

  public changeFavoriteChampion(championId: string, addFavorite: boolean): Observable<boolean> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.post<boolean>(
      environment.url_api + 'user/change-favorite-champion',
      {championId, addToFavorite: addFavorite},
      httpOptionsWithAuthorization
    );
  }

  public getFavoriteChampions(): Observable<Array<Champion>> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.get<Array<Champion>>(
      environment.url_api + 'user/get-favorite-champions',
      httpOptionsWithAuthorization
    );
  }

  public checkIfFavoriteSummoner(summonerName: string): Observable<boolean> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.post<boolean>(
      environment.url_api + 'user/check-favorite-summoner',
      summonerName,
      httpOptionsWithAuthorization
    );
  }

  public changeFavoriteSummoner(summonerName: string, summonerIcon: number, addFavorite: boolean): Observable<boolean> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.post<boolean>(
      environment.url_api + 'user/change-favorite-summoner',
      {summonerName, summonerIcon, addToFavorite: addFavorite},
      httpOptionsWithAuthorization
    );
  }

  public getFavoriteSummoners(): Observable<Array<Summoner>> {
    const httpOptionsWithAuthorization = this.getHttpOptionsWithAuthorization();

    return this.http.get<Array<Summoner>>(
      environment.url_api + 'user/get-favorite-summoner',
      httpOptionsWithAuthorization
    );
  }
}
