import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  numberTo404 = 0;

  constructor() { }

  async ngOnInit(): Promise<void> {
    while (this.numberTo404 < 404) {
      this.numberTo404++;
      await this.delay(5);
    }
  }

  // tslint:disable-next-line:typedef
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
