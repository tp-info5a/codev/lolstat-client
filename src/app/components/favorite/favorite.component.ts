import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Champion} from '../../models/champion';
import {ErrorApiModalComponent} from '../error-api-modal/error-api-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SummonerLight} from '../../models/summoner';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  typeFavorite: string;
  favoriteChampions: Array<Champion> = [];
  favoriteSummoner: Array<SummonerLight> = [];

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    if (window.localStorage.getItem('token') != null) {
      this.userService.getFavoriteChampions().subscribe(
        (data) => this.favoriteChampions = data,
        error => {
          console.error(error);
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );

      this.userService.getFavoriteSummoners().subscribe(
        (data) => this.favoriteSummoner = data,
        error => {
          console.error(error);
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );
    } else {
      this.router.navigateByUrl('/').then();
    }
  }

}
