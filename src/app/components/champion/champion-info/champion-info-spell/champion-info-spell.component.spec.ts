import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionInfoSpellComponent } from './champion-info-spell.component';

describe('ChampionInfoSpellComponent', () => {
  let component: ChampionInfoSpellComponent;
  let fixture: ComponentFixture<ChampionInfoSpellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionInfoSpellComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionInfoSpellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
