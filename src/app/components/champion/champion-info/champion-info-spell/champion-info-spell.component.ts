import {Component, Input, OnInit} from '@angular/core';
import {ChampionDataPassive, ChampionDataSpell} from '../../../../models/champion-data';

@Component({
  selector: 'app-champion-info-spell',
  templateUrl: './champion-info-spell.component.html',
  styleUrls: ['./champion-info-spell.component.scss']
})
export class ChampionInfoSpellComponent implements OnInit {

  @Input() partype: string;
  @Input() passive: ChampionDataPassive = new ChampionDataPassive();
  @Input() spells: Array<ChampionDataSpell> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
