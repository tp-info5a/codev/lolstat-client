import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionInfoComponent } from './champion-info.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ChampionService} from '../../../services/champion.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {NgbNavModule} from '@ng-bootstrap/ng-bootstrap';

describe('ChampionInfoComponent', () => {
  let component: ChampionInfoComponent;
  let fixture: ComponentFixture<ChampionInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionInfoComponent ],
      imports: [
        HttpClientTestingModule,
        RouterModule.forRoot([]),
        NgbNavModule
      ],
      providers: [ChampionService, HttpClient]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
