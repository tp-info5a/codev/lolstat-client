import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChampionService} from '../../../services/champion.service';
import {ChampionData} from '../../../models/champion-data';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {faAtlas, faDiceD20, faFistRaised, faInfoCircle, faPlus, faStar} from '@fortawesome/free-solid-svg-icons';
import {IconDefinition} from '@fortawesome/fontawesome-common-types';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-champion-info',
  templateUrl: './champion-info.component.html',
  styleUrls: ['./champion-info.component.scss']
})
export class ChampionInfoComponent implements OnInit {

  championData = new ChampionData();

  // font awesome icons
  faInfoCircle = faInfoCircle;
  faFistRaised = faFistRaised;
  faDiceD20 = faDiceD20;
  faAtlas = faAtlas;
  favoriteIcon: IconDefinition = faPlus;

  isConnected = false;

  constructor(private championService: ChampionService,  private route: ActivatedRoute, private modalService: NgbModal,
              private userService: UserService) { }

  ngOnInit(): void {

    if (window.localStorage.getItem('token') != null) {
      this.isConnected = true;
    }

    const championName = this.route.snapshot.paramMap.get('championName');
    this.championService.getChampionByName(championName).subscribe(
      data => {
        this.championData = data;
        this.championData.recommended.sort((a, b) => b.mode.localeCompare(a.mode));
        if (this.isConnected) {
          this.userService.checkIfFavoriteChampion(this.championData.id).subscribe(
            (dataBool) => dataBool ? this.favoriteIcon = faStar : this.favoriteIcon = faPlus,
            error => {
              const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
              ngbModalRef.componentInstance.error = error;
            }
          );
        }
      },
      error => {
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }

  changefavorite(): void {
    if (this.favoriteIcon === faStar) {
      this.userService.changeFavoriteChampion(this.championData.id, false).subscribe(
        () => this.favoriteIcon = faPlus,
        error => {
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );
    } else {
      this.userService.changeFavoriteChampion(this.championData.id, true).subscribe(
        () => this.favoriteIcon = faStar,
        error => {
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );
    }
  }
}
