import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionInfoRecommendedItemComponent } from './champion-info-recommended-item.component';

describe('ChampionInfoRecommendedItemComponent', () => {
  let component: ChampionInfoRecommendedItemComponent;
  let fixture: ComponentFixture<ChampionInfoRecommendedItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionInfoRecommendedItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionInfoRecommendedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
