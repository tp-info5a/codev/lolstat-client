import {Component, Input, OnInit} from '@angular/core';
import {ItemForChampion} from '../../../../models/item';

@Component({
  selector: 'app-champion-info-recommended-item',
  templateUrl: './champion-info-recommended-item.component.html',
  styleUrls: ['./champion-info-recommended-item.component.scss']
})
export class ChampionInfoRecommendedItemComponent implements OnInit {

  @Input() recommendedItem: ItemForChampion = new ItemForChampion();

  isCollapsed = true;

  constructor() { }

  ngOnInit(): void {
    this.isCollapsed = this.recommendedItem.mode !== 'CLASSIC';

    console.log(this.recommendedItem);
  }

}
