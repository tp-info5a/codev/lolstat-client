import {Component, Input, OnInit} from '@angular/core';
import {ItemBlock} from '../../../../../models/item';

@Component({
  selector: 'app-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.scss']
})
export class BlockComponent implements OnInit {

  @Input() blocks: Array<ItemBlock> = [];
  @Input() mode: string;
  @Input() isCollapsed: boolean;

  blocksJungle: Array<ItemBlock>;
  blocksLane: Array<ItemBlock>;

  constructor() { }

  ngOnInit(): void {
    this.blocksJungle = this.blocks.filter(block => block.showIfSummonerSpell === 'SummonerSmite');
    this.blocksLane = this.blocks.filter(block => block.showIfSummonerSpell !== 'SummonerSmite');
  }

}
