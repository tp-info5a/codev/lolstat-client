import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionInfoTipsComponent } from './champion-info-tips.component';

describe('ChampionInfoTipsComponent', () => {
  let component: ChampionInfoTipsComponent;
  let fixture: ComponentFixture<ChampionInfoTipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionInfoTipsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionInfoTipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
