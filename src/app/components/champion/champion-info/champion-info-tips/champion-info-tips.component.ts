import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-champion-info-tips',
  templateUrl: './champion-info-tips.component.html',
  styleUrls: ['./champion-info-tips.component.css']
})
export class ChampionInfoTipsComponent implements OnInit {

  @Input() championName: string;
  @Input() allytips: Array<string> = [];
  @Input() enemytips: Array<string> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
