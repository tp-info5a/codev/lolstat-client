import {Component, Input, OnInit} from '@angular/core';
import {ChampionDataSkin} from '../../../../models/champion-data';

@Component({
  selector: 'app-champion-info-presentation',
  templateUrl: './champion-info-presentation.component.html',
  styleUrls: ['./champion-info-presentation.component.scss']
})
export class ChampionInfoPresentationComponent implements OnInit {

  @Input() lore: string;
  @Input() skins: Array<ChampionDataSkin> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
