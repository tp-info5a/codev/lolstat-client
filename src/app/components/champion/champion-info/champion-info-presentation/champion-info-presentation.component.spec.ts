import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionInfoPresentationComponent } from './champion-info-presentation.component';

describe('ChampionInfoPresentationComponent', () => {
  let component: ChampionInfoPresentationComponent;
  let fixture: ComponentFixture<ChampionInfoPresentationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionInfoPresentationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionInfoPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
