import {Component, Input, OnInit} from '@angular/core';
import {Champion} from '../../../models/champion';

@Component({
  selector: 'app-champion-card',
  templateUrl: './champion-card.component.html',
  styleUrls: ['./champion-card.component.css']
})
export class ChampionCardComponent implements OnInit {

  @Input() championList: Array<Champion>;

  constructor() { }

  ngOnInit(): void {
  }

  infoChampion(champion: Champion): void {
    console.log(champion);
  }
}
