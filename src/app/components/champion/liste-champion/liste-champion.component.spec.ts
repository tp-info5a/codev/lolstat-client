import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeChampionComponent } from './liste-champion.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ChampionService} from '../../../services/champion.service';
import {HttpClient} from '@angular/common/http';

describe('ListeChampionComponent', () => {
  let component: ListeChampionComponent;
  let fixture: ComponentFixture<ListeChampionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeChampionComponent ],
      imports: [HttpClientTestingModule],
      providers: [ChampionService, HttpClient]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeChampionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
