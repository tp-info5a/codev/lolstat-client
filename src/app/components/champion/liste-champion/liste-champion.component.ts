import { Component, OnInit } from '@angular/core';
import {ChampionService} from '../../../services/champion.service';
import {Champion} from '../../../models/champion';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';

@Component({
  selector: 'app-liste-champion',
  templateUrl: './liste-champion.component.html',
  styleUrls: ['./liste-champion.component.css']
})
export class ListeChampionComponent implements OnInit {

  championsList: Array<Champion> = [];
  filteredChampionsList: Array<Champion> = [];
  freeChampionsList: Array<Champion> = [];

  search = '';

  constructor(private championService: ChampionService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.championService.getAllChampion().subscribe(
      data => {
        this.championsList = data;
        this.filteredChampionsList = data;
        this.freeChampionsList = data.filter(champion => champion.free);
      },
      error => {
        console.error(error);
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }

  filterChampionList(): void {
    this.filteredChampionsList = this.championsList.filter(champion => champion.name.toUpperCase().includes(this.search.toUpperCase()));
  }

}
