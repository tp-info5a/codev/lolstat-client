import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SignInUser} from '../../../models/user';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';
import {UserService} from '../../../services/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css']
})
export class SignInFormComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private modalService: NgbModal,
              private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      mail: ['', [Validators.required, Validators.email]],
      pwd: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f(): any { return this.form.controls; }

  submit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    const user = new SignInUser();

    user.mail = this.form.controls.mail.value;
    user.password = this.form.controls.pwd.value;

    this.userService.signInUser(user).subscribe(
      (tokenJSON) => {
        window.localStorage.setItem('token', tokenJSON.token);
        this.router.navigateByUrl('/').then(() => location.reload());
      },
      error => {
        console.error(error);
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }
}
