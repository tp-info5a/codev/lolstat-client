import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegisterUser} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private modalService: NgbModal,
              private router: Router) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validators: this.mustMatch('password', 'confirmPassword')
    });
  }

  // custom validator to check that two fields match
  mustMatch(controlName: string, matchingControlName: string): any {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  // convenience getter for easy access to form fields
  get f(): any { return this.form.controls; }

  submit(): void {
    this.submitted = true;

    console.log(this.form);

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    const newUser = new RegisterUser();

    newUser.firstName = this.form.controls.firstName.value;
    newUser.lastName = this.form.controls.lastName.value;
    newUser.email = this.form.controls.email.value;
    newUser.password = this.form.controls.password.value;

    this.userService.registerUser(newUser).subscribe(
      (tokenJSON) => {
        window.localStorage.setItem('token', tokenJSON.token);
        this.router.navigateByUrl('/').then(() => location.reload());
      },
      error => {
        console.error(error);
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }
}
