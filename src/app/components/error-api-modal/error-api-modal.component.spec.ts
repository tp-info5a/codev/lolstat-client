import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorApiModalComponent } from './error-api-modal.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

describe('ErrorApiModalComponent', () => {
  let component: ErrorApiModalComponent;
  let fixture: ComponentFixture<ErrorApiModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErrorApiModalComponent ],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorApiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
