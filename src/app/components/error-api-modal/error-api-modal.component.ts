import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-error-api-modal',
  templateUrl: './error-api-modal.component.html',
  styleUrls: ['./error-api-modal.component.css']
})
export class ErrorApiModalComponent implements OnInit {

  @Input() error: any = {
    error: '',
    message: ''
  };

  constructor(private modal: NgbActiveModal) { }

  ngOnInit(): void {}

  close(): void {
    this.modal.close('close');
  }
}
