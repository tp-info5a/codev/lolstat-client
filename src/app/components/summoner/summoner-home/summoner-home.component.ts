import { Component, OnInit } from '@angular/core';
import {Summoner, SummonerMasteries} from '../../../models/summoner';
import {ActivatedRoute} from '@angular/router';
import {SummonerService} from '../../../services/summoner.service';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {faPlus, faStar} from '@fortawesome/free-solid-svg-icons';
import {IconDefinition} from '@fortawesome/fontawesome-common-types';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-summoner-home',
  templateUrl: './summoner-home.component.html',
  styleUrls: ['./summoner-home.component.css']
})
export class SummonerHomeComponent implements OnInit {

  summoner: Summoner = new Summoner();
  summonerMasteryList: Array<SummonerMasteries> = [];

  isConnected = false;

  favoriteIcon: IconDefinition = faPlus;

  constructor(private route: ActivatedRoute, private summonerService: SummonerService, private modalService: NgbModal,
              private userService: UserService) {
  }

  ngOnInit(): void {
    if (window.localStorage.getItem('token') != null) {
      this.isConnected = true;
    }

    const summonerName = this.route.snapshot.paramMap.get('summonerName');

    this.summonerService.getSummonerByName(summonerName).subscribe(
      (data) => {
        this.summoner = data;
        this.summonerService.getSummonerMasteries(this.summoner.id).subscribe(
          (data2) => {
            this.summonerMasteryList = data2;
          },
          (error) => {
            const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
            ngbModalRef.componentInstance.error = error;
          }
        );
        if (this.isConnected) {
          this.userService.checkIfFavoriteSummoner(this.summoner.id).subscribe(
            (dataBool) => dataBool ? this.favoriteIcon = faStar : this.favoriteIcon = faPlus,
            error => {
              const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
              ngbModalRef.componentInstance.error = error;
            }
          );
        }
      },
      (error) => {
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }

  changefavorite(): void {
    if (this.favoriteIcon === faStar) {
      this.userService.changeFavoriteSummoner(this.summoner.name, this.summoner.profileIconId, false).subscribe(
        () => this.favoriteIcon = faPlus,
        error => {
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );
    } else {
      this.userService.changeFavoriteSummoner(this.summoner.name, this.summoner.profileIconId, true).subscribe(
        () => this.favoriteIcon = faStar,
        error => {
          const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
          ngbModalRef.componentInstance.error = error;
        }
      );
    }
  }

}
