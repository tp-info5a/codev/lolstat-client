import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummonerHomeComponent } from './summoner-home.component';

describe('SummonerHomeComponent', () => {
  let component: SummonerHomeComponent;
  let fixture: ComponentFixture<SummonerHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummonerHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummonerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
