import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../../models/item';
import {SummonerService} from '../../../services/summoner.service';
import {Matches} from '../../../models/matches';
import {ErrorApiModalComponent} from '../../error-api-modal/error-api-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-summoner-match',
  templateUrl: './summoner-match.component.html',
  styleUrls: ['./summoner-match.component.css']
})
export class SummonerMatchComponent implements OnInit {

  @Input() accountId: string;

  matchHistory: Matches = new Matches();

  page = 1;

  constructor(private summonerService: SummonerService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.onPageChange(this.page);
  }

  onPageChange(page: any): void {
    this.summonerService.getSummonerMatchHistory(this.accountId, page).subscribe(
      (data) => this.matchHistory = data,
      (error) => {
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }
}
