import {Component, Input, OnInit} from '@angular/core';
import {Game, MatchDetail, Participant} from '../../../../models/matches';
import {ErrorApiModalComponent} from '../../../error-api-modal/error-api-modal.component';
import {SummonerService} from '../../../../services/summoner.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Item} from '../../../../models/item';
import {Observable, Subject} from 'rxjs';
import {Champion} from '../../../../models/champion';
import {ChampionService} from '../../../../services/champion.service';

@Component({
  selector: 'app-summoner-match-detail',
  templateUrl: './summoner-match-detail.component.html',
  styleUrls: ['./summoner-match-detail.component.css']
})
export class SummonerMatchDetailComponent implements OnInit {

  @Input() game: Game;

  detail: MatchDetail = new MatchDetail();
  participant: Participant = new Participant();
  champion: Champion = new Champion();
  items: Array<Item> = [];
  victory = 'success';
  mapName: string;

  constructor(private summonerService: SummonerService, private championService: ChampionService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.summonerService.getMatchDetail(this.game.gameId).subscribe(
      (data) => {
        this.detail = data;

        this.championService.getChampionById(this.game.champion).subscribe(
          (data2) => this.champion = data2,
          (error) => {
            const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
            ngbModalRef.componentInstance.error = error;
          }
        );

        this.summonerService.getMapName(this.detail.mapId).subscribe(
          (data2) => this.mapName = data2.mapName,
          (error) => {
            const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
            ngbModalRef.componentInstance.error = error;
          }
        );

        this.participant = this.detail.participants.find((participant) => participant.championId === this.game.champion);

        if (this.participant.stats.win) {
          this.victory = 'success';
        } else {
          this.victory = 'danger';
        }

        this.checkAndGetItem(this.participant.stats.item0);
        this.checkAndGetItem(this.participant.stats.item1);
        this.checkAndGetItem(this.participant.stats.item2);
        this.checkAndGetItem(this.participant.stats.item3);
        this.checkAndGetItem(this.participant.stats.item4);
        this.checkAndGetItem(this.participant.stats.item5);
        this.checkAndGetItem(this.participant.stats.item6);
      },
      (error) => {
        const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        ngbModalRef.componentInstance.error = error;
      }
    );
  }

  checkAndGetItem(itemId: number): void {
    if (itemId !== 0) {
      this.getItem(itemId).subscribe(item => this.items.push(item));
    }
  }

  getItem(itemId: number): Observable<Item> {
    const subject = new Subject<Item>();
    this.summonerService.getItem(itemId).subscribe(
      (data) => subject.next(data),
      (error) => {
        console.log(error);
        // const ngbModalRef = this.modalService.open(ErrorApiModalComponent);
        // ngbModalRef.componentInstance.error = error;
      }
    );

    return subject.asObservable();
  }

}
