import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummonerMatchDetailComponent } from './summoner-match-detail.component';

describe('SummonerMatchDetailComponent', () => {
  let component: SummonerMatchDetailComponent;
  let fixture: ComponentFixture<SummonerMatchDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummonerMatchDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummonerMatchDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
