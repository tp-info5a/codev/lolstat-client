import {Component, Input, OnInit} from '@angular/core';
import {SummonerLight} from '../../../models/summoner';

@Component({
  selector: 'app-summoner-card',
  templateUrl: './summoner-card.component.html',
  styleUrls: ['./summoner-card.component.css']
})
export class SummonerCardComponent implements OnInit {

  @Input() summonerLightList: Array<SummonerLight> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
