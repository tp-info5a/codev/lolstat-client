import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListeChampionComponent} from './components/champion/liste-champion/liste-champion.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {HomeComponent} from './components/home/home.component';
import {ChampionInfoComponent} from './components/champion/champion-info/champion-info.component';
import {SignInFormComponent} from './components/forms/sign-in-form/sign-in-form.component';
import {RegisterFormComponent} from './components/forms/register-form/register-form.component';
import {FavoriteComponent} from './components/favorite/favorite.component';
import {SummonerHomeComponent} from './components/summoner/summoner-home/summoner-home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'champion', component: ListeChampionComponent },
  { path: 'champion/:championName', component: ChampionInfoComponent },
  { path: 'login', component: SignInFormComponent},
  { path: 'register', component: RegisterFormComponent},
  { path: 'favorite', component: FavoriteComponent},
  { path: 'summoner/:summonerName', component: SummonerHomeComponent},

  { path: 'houston-we-have-a-problem', component: NotFoundComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
