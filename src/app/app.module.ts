import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { ListeChampionComponent } from './components/champion/liste-champion/liste-champion.component';
import {HttpClientModule} from '@angular/common/http';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ErrorApiModalComponent } from './components/error-api-modal/error-api-modal.component';
import { HomeComponent } from './components/home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ChampionInfoComponent } from './components/champion/champion-info/champion-info.component';
import { ChampionCardComponent } from './components/champion/champion-card/champion-card.component';
import { ChampionInfoSpellComponent } from './components/champion/champion-info/champion-info-spell/champion-info-spell.component';
import { ChampionInfoTipsComponent } from './components/champion/champion-info/champion-info-tips/champion-info-tips.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ChampionInfoRecommendedItemComponent } from './components/champion/champion-info/champion-info-recommended-item/champion-info-recommended-item.component';
import { ChampionInfoPresentationComponent } from './components/champion/champion-info/champion-info-presentation/champion-info-presentation.component';
import { BlockComponent } from './components/champion/champion-info/champion-info-recommended-item/block/block.component';
import { SignInFormComponent } from './components/forms/sign-in-form/sign-in-form.component';
import { RegisterFormComponent } from './components/forms/register-form/register-form.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { SummonerHomeComponent } from './components/summoner/summoner-home/summoner-home.component';
import { SummonerMatchComponent } from './components/summoner/summoner-match/summoner-match.component';
import { SummonerCardComponent } from './components/summoner/summoner-card/summoner-card.component';
import { SummonerMatchDetailComponent } from './components/summoner/summoner-match/summoner-match-detail/summoner-match-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeChampionComponent,
    NotFoundComponent,
    ErrorApiModalComponent,
    HomeComponent,
    ChampionInfoComponent,
    ChampionCardComponent,
    ChampionInfoSpellComponent,
    ChampionInfoTipsComponent,
    ChampionInfoRecommendedItemComponent,
    ChampionInfoPresentationComponent,
    BlockComponent,
    SignInFormComponent,
    RegisterFormComponent,
    FavoriteComponent,
    SummonerHomeComponent,
    SummonerMatchComponent,
    SummonerCardComponent,
    SummonerMatchDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule
  ],
  providers: [NgbActiveModal],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
