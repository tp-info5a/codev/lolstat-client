import {HttpHeaders} from '@angular/common/http';

export const environment = {
  production: true,
  url_api: 'http://143.110.173.27:8080/api/',
  httpOptions: {
    headers: new HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://lolstat.armanet.fr/'
    })
  }
};
